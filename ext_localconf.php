<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveExtHistory',
            'Hivehistorylisthistory',
            [
                'History' => 'list'
            ],
            // non-cacheable actions
            [
                'History' => 'list'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveExtHistory',
            'Hivehistoryshowhistory',
            [
                'History' => 'list, show'
            ],
            // non-cacheable actions
            [
                'History' => 'list, show'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hivehistorylisthistory {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_ext_history') . 'Resources/Public/Icons/user_plugin_hivehistorylisthistory.svg
                        title = LLL:EXT:hive_ext_history/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_history_domain_model_hivehistorylisthistory
                        description = LLL:EXT:hive_ext_history/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_history_domain_model_hivehistorylisthistory.description
                        tt_content_defValues {
                            CType = list
                            list_type = hiveexthistory_hivehistorylisthistory
                        }
                    }
                    hivehistoryshowhistory {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_ext_history') . 'Resources/Public/Icons/user_plugin_hivehistoryshowhistory.svg
                        title = LLL:EXT:hive_ext_history/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_history_domain_model_hivehistoryshowhistory
                        description = LLL:EXT:hive_ext_history/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_history_domain_model_hivehistoryshowhistory.description
                        tt_content_defValues {
                            CType = list
                            list_type = hiveexthistory_hivehistoryshowhistory
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
