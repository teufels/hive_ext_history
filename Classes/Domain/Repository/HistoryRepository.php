<?php
namespace HIVE\HiveExtHistory\Domain\Repository;

/***
 *
 * This file is part of the "hive_ext_history" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *           Yannick Aister <y.aister@teufels.com>, teufels GmbH
 *
 ***/

/**
 * The repository for Historys
 */
class HistoryRepository extends \HIVE\HiveCfgRepository\Domain\Repository\AbstractRepository
{
    public function initializeObject()
    {
        $sUserFuncModel = 'HIVE\\HiveExtHistory\\Domain\\Model\\History';
        $sUserFuncPlugin = 'tx_hiveexthistory';
        parent::overrideQuerySettings($sUserFuncModel, $sUserFuncPlugin);
    }
    /**
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    /**
     * findByUidListOrderByListIfNotEmpty
     *
     * @param string String containing uids
     * @return \HIVE\HiveExtHistory\Domain\Model\History
     */
    public function findByUidListOrderByListIfNotEmpty($uidList)
    {
        $uidArray = explode(',', $uidList);
        $result = [];
        foreach ($uidArray as $uid) {
            $result[] = $this->findByUid($uid);
        }

        //return only if not empty
        if ($result[0] != NULL){
            return $result;
        }
    }

    /**
     * Find by Uid
     *
     * @param int $uid
     * @return History
     */
    public function findByUid($uid)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->matching($query->equals('uid', $uid));
        return $query->execute()->getFirst();
    }

//    /**
//     *
//     * Returns all objects of this repository with matching category
//     *
//     * @param \HIVE\HiveExtHistory\Domain\Model\History $category
//     *
//     * @return \HIVE\HiveExtHistory\Domain\Model\History
//     */
//    public function findByCategory(Tx_HiveExtHistory_Domain_Model_History $category) {
//        $query = $this->createQuery();
//        $query->matching($query->contains('categories', $category));
//        $result = $query->execute();
//        return $result;
//    }
}