<?php

if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

$sModel = 'tx_hiveexthistory_domain_model_history';

$GLOBALS['TCA'][$sModel]['columns']['title'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:hive_ext_article/Resources/Private/Language/locallang_db.xlf:tx_hiveextarticle_domain_model_article.title',
    'config' => [
        'type' => 'input',
        'size' => 30,
        'eval' => 'trim'
    ],
];


$GLOBALS['TCA'][$sModel]['columns']['text'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:hive_ext_article/Resources/Private/Language/locallang_db.xlf:tx_hiveextarticle_domain_model_article.text',
    'config' => [
        'type' => 'text',
        'cols' => 40,
        'rows' => 15,
        'eval' => 'trim',
    ],
    'defaultExtras' => 'richtext:rte_transform'
];

$GLOBALS['TCA'][$sModel]['columns']['meta_data'] = [
    'exclude' => false,
    'label' => 'LLL:EXT:hive_ext_article/Resources/Private/Language/locallang_db.xlf:tx_hiveextarticle_domain_model_article.meta_data',
    'config' => [
        'type' => 'inline',
        'foreign_table' => 'tx_hiveextmetadata_domain_model_metadata',
        'minitems' => 0,
        'maxitems' => 1,
        'appearance' => [
            'collapseAll' => 0,
            'levelLinksPosition' => 'top',
            'showSynchronizationLink' => 1,
            'showPossibleLocalizationRecords' => 1,
            'showAllLocalizationLink' => 1
        ],
    ],
];

//$GLOBALS['TCA'][$sModel]['columns']['taxonomy'] = [
//    'exclude' => false,
//    'label' => 'LLL:EXT:hive_ext_article/Resources/Private/Language/locallang_db.xlf:tx_hiveextarticle_domain_model_article.taxonomy',
//    'config' => [
//        'type' => 'select',
//        'renderType' => '',
//        'foreign_table' => 'tx_hiveexttaxonomy_domain_model_tag',
//        'MM' => 'tx_hiveextarticle_article_tag_mm',
//        'size' => 10,
//        'autoSizeMax' => 30,
//        'maxitems' => 9999,
//        'multiple' => 0,
//        'wizards' => [
//            '_PADDING' => 1,
//            '_VERTICAL' => 1,
//            'edit' => [
//                'module' => [
//                    'name' => 'wizard_edit',
//                ],
//                'type' => 'popup',
//                'title' => 'Edit', // todo define label: LLL:EXT:.../Resources/Private/Language/locallang_tca.xlf:wizard.edit
//                'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_edit.gif',
//                'popup_onlyOpenIfSelected' => 1,
//                'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
//            ],
//            'add' => [
//                'module' => [
//                    'name' => 'wizard_add',
//                ],
//                'type' => 'script',
//                'title' => 'Create new', // todo define label: LLL:EXT:.../Resources/Private/Language/locallang_tca.xlf:wizard.add
//                'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_add.gif',
//                'params' => [
//                    'table' => 'tx_hiveexttaxonomy_domain_model_tag',
//                    'pid' => '###CURRENT_PID###',
//                    'setValue' => 'prepend'
//                ],
//            ],
//        ],
//    ],
//];

$GLOBALS['TCA'][$sModel]['types']['1']['showitem'] =
    '--div--;General,sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource,
    --div--;History, title, subtitle, text, detailtext,
    --div--;Image,image,
    --div--;Metadata,meta_data'
;


$GLOBALS['TCA'][$sModel]['ctrl']['label'] = 'title';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt'] = 'subtitle';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt_force'] =  1;